# sreview

Installs and configures the sreview video reviewing system.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

There are no variables for this role.
