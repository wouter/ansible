veyepar-secrets
---------------

Copies files from CarlFK's laptop to machines that will be doing veyepar
encoding.

This is a legacy role and should ideally be replaced by use of Ansible
Vault.
